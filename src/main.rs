use ring_buffer::*;

fn main() {
    let mut b = RingBuffer::new(100);

    let r1 = b.push(10);
    let r2 = b.pull();

    println!("{r1:?} {r2:?} \n{b:?}\n {:?}", b.is_empty());
}
