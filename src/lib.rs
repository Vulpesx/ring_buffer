use std::cell::Cell;

#[derive(Debug)]
pub struct RingBuffer<T: Clone> {
    buffer: Vec<Option<T>>,
    read_idx: Cell<usize>,
    write_idx: usize,
}

#[derive(Debug)]
pub struct Full;

impl<T: Clone> RingBuffer<T> {
    /// create a `RingBuffer` with size
    pub fn new(size: usize) -> Self {
        Self {
            buffer: vec![None; size],
            read_idx: Cell::new(0),
            write_idx: 0,
        }
    }

    /// add an item to the buffer
    pub fn push(&mut self, item: T) -> Result<(), Full> {
        if self.is_full() {
            return Err(Full);
        }
        self.buffer[self.write_idx] = Some(item);
        self.write_idx = self.advance_idx(self.write_idx);
        Ok(())
    }

    /// get an item and remove it from the buffer
    pub fn pull(&mut self) -> Option<T> {
        let o = self.buffer[self.read_idx.get()].take();
        self.read_idx.set(self.advance_idx(self.read_idx.get()));
        o
    }

    /// gets an item without removing it
    pub fn get(&self) -> Option<T> {
        let o = self.buffer[self.read_idx.get()].clone();
        self.read_idx.set(self.advance_idx(self.read_idx.get()));
        o
    }

    /// gets an item without removing it or advancing through the buffer
    pub fn peek(&self) -> Option<T> {
        self.buffer[self.read_idx.get()].clone()
    }

    /// checks if buffer is full
    pub fn is_full(&self) -> bool {
        // if write index has encoutered `Some` it must have looped,
        // it should be impossible for the buffer not to be full if write
        // points to `Some` unless it somehow skipped an index which shouldnt happen
        self.buffer[self.write_idx].is_some()
    }

    /// increases the index by 1 and loops when reaching the end
    fn advance_idx(&self, idx: usize) -> usize {
        // increases index by 1, if it reaches capacity % returns 0,
        (idx + 1) % self.capacity()
    }

    /// returns the len of the buffer as that is the capacity
    /// this is just for ease of use
    pub fn capacity(&self) -> usize {
        // len is capacity this is just to make it easier to understand
        self.buffer.len()
    }

    /// checks if the buffer is empty
    pub fn is_empty(&self) -> bool {
        // this will only be none if there is nothing in the buffer
        // once something is pushed this will be some, read and write index start at 0
        // when pulling you remove the item you get and read is advanced,
        // if the next item is none it means that write didnt get to that index before read,
        // meaning the buffer is empty
        self.buffer[self.read_idx.get()].is_none()
    }
}
